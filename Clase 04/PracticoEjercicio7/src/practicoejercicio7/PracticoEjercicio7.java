/*
 * Crea un array o arreglo unidimensional con un tamaño de 5, asignar los valores
 * numéricos manualmente (los que tu quieras) y mostrarlos por pantalla utilizando las
 * estructuras cíclicas vistas en clase.
 */
package practicoejercicio7;

public class PracticoEjercicio7 {

    public static void main(String[] args) {
        int[] numeros = new int[5];

        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = (int) Math.round(Math.random() * 100);
        }
        for (int i = 0; i < numeros.length; i++) {
            System.out.println(numeros[i]);
        }
    }
}