package practicoejercicio2;

import java.util.Scanner;

public class PracticoEjercicio2 {

    public static void main(String[] args) {
        
        int n1, n2, opcion;
        float resDivision = 0;

        System.out.println("Ingrese 2 valores para sumar, restar, multiplicar y dividir:");
        Scanner obj = new Scanner(System.in);
        n1 = Integer.parseInt(obj.nextLine());
        n2 = Integer.parseInt(obj.nextLine());

        System.out.println("Suma: " + (n1 + n2));
        System.out.println("Resta: " + (n1 - n2));
        System.out.println("Multiplicacion: " + (n1 * n2));
        if (n2 != 0) {
            resDivision = n1 / n2;
            System.out.println("Division: " + resDivision);
        } else {
            System.out.println("Error al dividir por 0");
        }
    }

}
