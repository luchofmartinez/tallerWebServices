/*
 * Desarrollar un programa Java que sume los números del 1 al 100 (ambos inclusive).
 */
package practicoejercicio6;

public class PracticoEjercicio6 {

    public static void main(String[] args) {
        int total = 0;
        for(int i = 1; i<=100; i++){
            total += i;
        }
        System.out.println("Total: "+total);
    }
    
}
