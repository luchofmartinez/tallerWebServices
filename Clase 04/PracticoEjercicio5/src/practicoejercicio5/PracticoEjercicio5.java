/*
 * Desarrollar un programa Java que calcule el factorial de un número entero.
 */
package practicoejercicio5;

public class PracticoEjercicio5 {

    public static void main(String[] args) {
        int number = 5;
        int factorial = 1;
        for(int i=2; i<=number; i++){
            factorial = factorial * i;
        }
        System.out.println(factorial);
    }
    
}
