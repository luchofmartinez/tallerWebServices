/*
 * Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor de los
 * dos. Si son iguales indicarlo también. Ves cambiando los valores para comprobar que
 * funciona.
 */
package practicoejercicio3;

public class PracticoEjercicio3 {

    public static void main(String[] args) {
        int v_numero1 = 7;
        int v_numero2 = 7;

        if (v_numero1 == v_numero2) {
            System.out.println("Son iguales");
        } else {
            if (v_numero1 > v_numero2) {
                System.out.println(v_numero1 + " es mayor a " + v_numero2);
            } else {
                System.out.println(v_numero1 + " es menor a " + v_numero2);
            }
        }
    }

}
