package vectorpractico1;

public class Tratamiento {

    private int[] vecPositivos;
    private int[] vecNegativos;
    private int[] numeros;
    private Vector v1;
    private int totalPositivos;
    private int totalNegativos;

    public Tratamiento(int longitud) {
        v1 = new Vector();
        numeros = v1.getVecNumeros();
        vecPositivos = new int[longitud];
        vecNegativos = new int[longitud];
        totalPositivos = 0;
        totalNegativos = 0;
    }

    public void tratarNumeros() {
        int posPositiva = 0;
        int posNegativa = 0;
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] >= 0) {
                vecPositivos[posPositiva] = numeros[i];
                totalPositivos += numeros[i];
                posPositiva++;
            } else {
                vecNegativos[posNegativa] = numeros[i];
                totalNegativos += numeros[i];
                posNegativa++;
            }
        }
    }

    public String toStringPositivos() {
        String cadena = "";
        for (int  i = 0; i < vecPositivos.length;i++){
            
                cadena += vecPositivos[i] + " | ";
            
        }
        return cadena;
    }
    
        public String toStringNegativos() {
        String cadena = "";
        for (int  i = 0; i < vecNegativos.length;i++){
            
                cadena += vecNegativos[i] + " | ";
            
        }
        return cadena;
    }
        
        public int sumaPositivos(){
            return totalPositivos;
        }
        
        public int sumaNegativos(){
            return totalNegativos;
        }
        
        public int totalTodo(){
            return totalPositivos + totalNegativos;
        }
}
