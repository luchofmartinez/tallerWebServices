package vectorpractico1;

import java.util.Scanner;

public class VectorPractico1 {

    public static void main(String[] args) {
        Tratamiento t1 = new Tratamiento(10);

        t1.tratarNumeros();
        System.out.println("Valores negativos: "+t1.toStringNegativos());
        System.out.println("Valores positivos: "+t1.toStringPositivos());
        System.out.println("");
        System.out.println("Punto D");
        System.out.println("Suma de positivos: "+t1.sumaPositivos());
        System.out.println("");
        System.out.println("Punto E");
        System.out.println("Suma de negativos: "+t1.sumaNegativos());
        System.out.println("");
        System.out.println("Suma total de positivos y negativos: "+t1.totalTodo());
        System.out.println("");
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese 3 valores: ");
        int valor1 = scan.nextInt();
        int valor2 = scan.nextInt();
        int valor3 = scan.nextInt();
        
        System.out.println("Valores ingresados: "+valor1+" || "+valor2+" || "+valor3);
    }

}
