package vectorpractico1;

public class Vector {
    
    private int [] vecNumeros = {11,-22,33,-44,55,-66,77,-88,99};

    public Vector() {
    }

    public int[] getVecNumeros() {
        return vecNumeros;
    }

    public void setVecNumeros(int[] vecNumeros) {
        this.vecNumeros = vecNumeros;
    }
    
    
}
