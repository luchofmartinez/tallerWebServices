package objetoclasestatic;

public class Clase {
    private static double PI = Math.PI;

    public Clase() {
    }   

    public static double getPI() {
        return PI;
    }

    public static void setPI(double PI) {
        Clase.PI = PI;
    }
    
}
