package objetoclasestatic;

public class ObjetoClaseStatic {

    public static void main(String[] args) {
        
        Clase c1 = new Clase();
        Clase c2 = new Clase();
        Clase c3 = new Clase();
        
        System.out.println("Valor de c1: "+c1.getPI());
        System.out.println("Seteo el valor de c2: Valor actual -> "+c2.getPI());
        c1.setPI(6.28); // S E T E O   VARIABLE 
        System.out.println("Valor de c2 luego: "+c2.getPI());
        System.out.println("Valor de c3: "+c3.getPI());
    }
}