package practico1;

public class Circulo {
    private Punto centro;
    private int radio;
    private final double pi = Math.PI;
    
    /**
     *  Constructor de la Clase
     */
    public Circulo(){ 
        this.centro = new Punto(0, 0);
        this.radio=0;
    }
    /**
     * Constructor con 2 parametros de la Clase
     * @param punto
     * @param radio 
     */
    public Circulo(Punto punto, int radio){ 
        this.centro = punto;    
        this.radio = radio;
    }
    /**
     * Constructor con 3 parametros de la Clase
     * @param x
     * @param y
     * @param radio 
     */
    public Circulo(int x, int y, int radio) {
        //super(x,y);
        this.centro = new Punto(x, y);
        this.radio=radio;
    }
    
    /**
     * Setea el valor de la coordenada x
     * @param x coordenada x del centro del circulo
     */
    public void setX(int x) {
        this.centro.setX(x);
    }
    
    /**
     * Setea el valor de la coordenada y 
     * @param y coordenada y del centro del circulo
     */
    public void setY(int y) {
        this.centro.setY(y);
    }
    /**
     * Setea el valor del radio 
     * @param radio 
     */
    public void setRadio(int radio) {
        this.radio=radio;
    }
    /**
     * Obtiene el valor de la coordenada x del centro del circulo
     * @return 
     */
    public int getX() {
        return this.centro.getX();
    }
    /**
     * Obtiene el valor de la coordenada y del centro del circulo
     * @return 
     */
    public int getY() {
        return this.centro.getY();
    }
    /**
     * Obtiene el valor del radio del circulo
     * @return 
     */
    public int getRadio() {
        return this.radio;
    }
    public double calcularDistancia(Punto p2) {
        //Usamos el metodo para calcular distancia
        return centro.calcularDistanciaDesde(p2);
    }
    public double calcularArea(){
        //Calculamos el area pi*2^2
        return Math.pow(radio, 2)*Math.PI;
    } 
    public double calcularPerimetro(){
        //Calculamos el perimetro  2*pi*r
        return 2*pi*radio;
    }
}
