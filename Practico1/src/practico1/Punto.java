package practico1;

/**
 *
 * @author Luciano Martinez
 * @see <a href="https://tienda.claro.com.ar"> La tienda de mimi </a>
 * @version 1.0
 * @since 2017
 */
public class Punto {

    private int x;
    private int y;

    public Punto() {

    }

    /**
     * @param x Recibe un entero para setear el valor de la coordenada x
     * @param y Recibe un entero para setear el valor de la coordenada y
     */
    public Punto(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return Retorna el valor de la coordenada x
     */
    public int getX() {
        return this.x;
    }

    /**
     * @param x Setea el valor de la coordenada x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return Retorna el valor de la coordenada y
     */
    public int getY() {
        return this.y;
    }

    /**
     *
     * @param y Setea el valor de la coordenada y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     *
     * @param p Se recibe un objeto de tipo Punto.
     * @return Retorna la distancia entre el punto recibido y el punto creado.
     */
    public double calcularDistanciaDesde(Punto p) {
        double distancia = 0;
        double suma = Math.pow(p.getX() - this.x, 2) + Math.pow(p.getY() - this.y, 2);
        distancia = Math.sqrt(suma);
        return distancia;
    }
}
