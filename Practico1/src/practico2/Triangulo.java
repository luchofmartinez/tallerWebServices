package practico2;

public class Triangulo {
        private Punto a,b,c;
        
        public Triangulo() {
            this.a= new Punto();
            this.b= new Punto();
            this.c= new Punto();
        }

        public Triangulo(Punto a, Punto b, Punto c) {
            this.a = new Punto(a.getX(), a.getY());
            this.b = new Punto(b.getX(), b.getY());
            this.c = new Punto(c.getX(), c.getY());
            
        }

        public Triangulo(int a1, int a2, int b1, int b2, int c1, int c2){
            this.a = new Punto(a1, a2);
            this.b = new Punto(b1, b2);
            this.c = new Punto(c1, c2);
            
        }

        public void setXforA(int x) {
            this.a.setX(x);
        }

        public void setYforA(int y) {
            this.a.setY(y);
        }

        public void setXforB(int x) {
            this.b.setX(x);
        }

        public void setYforB(int y) {
            this.b.setY(y);
        }

        public void setXforC(int x) {
            this.c.setX(x);
        }

        public void setYforC(int y) {
            this.c.setY(y);
        }

        public double calcularDistancia(Punto p2){
            //Calculamos el punto centro del triangulo
            double centerX = (a.getX() + b.getX() + c.getX())/3;
            double centerY = (a.getY() + b.getY() + c.getY())/3;
            //Creamos el centro tipo Punto
            Punto centroTriangulo = new Punto ((int)centerX, (int)centerY);
            //Usamos el metodo para calcular distancia
            return centroTriangulo.calcularDistancia(p2);
        }

        public double calcularArea(){
            //Usamos matriz
            double  area = ( Math.abs( 
                    a.getX()*b.getY() + 
                    b.getX()*c.getY() +
                    c.getX()*a.getY() )
                    )/2;
            return area;
        }

        public double calcularPerimetro(){
            return (// El perimetro de un triangulo es: 'perimetro = AB + BC + CA'
                    // calculamos distancia AB 
                    a.calcularDistancia(b) +
                    // calculamos distancia BC
                    b.calcularDistancia(c) + 
                    // calculamos distancia CA
                    c.calcularDistancia(a)
                    ); 
        }
        
}
