package practico2;

public class Punto {
    private int x,y;
    
    public Punto() {
        x = 0;
        y = 0;
    }

    public Punto(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
            this.x = x;
        }

    public void setY(int y) {
            this.y = y;
        }

    public int getX(){
            return x;
        }

    public int getY(){
            return y;
        }

    public double calcularDistancia(Punto p2){
        double distancia = Math.sqrt( 
                ((p2.getX()-this.x) * (p2.getX()-this.x)) + 
                ((p2.getY()-this.y) * (p2.getY()-this.y)) 
             );
        return distancia;
    }
}
