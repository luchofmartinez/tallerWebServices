package practico2;

public class Circulo {
    private Punto centro;
    private int radio;
    private final double pi = Math.PI;
    
    /*
    *
    */
    public Circulo(){ 
        this.centro = new Punto(0, 0);
        this.radio=0;
    }
    public Circulo(Punto punto, int radio){ 
        this.centro = punto;    
        this.radio = radio;
    }
    public Circulo(int x, int y, int radio) {
        //super(x,y);
        this.centro = new Punto(x, y);
        this.radio=radio;
    }
    
    public void setX(int x) {
        this.centro.setX(x);
    }
    public void setY(int y) {
        this.centro.setY(y);
    }
    public void setRadio(int radio) {
        this.radio=radio;
    }
    
    public int getX() {
        return this.centro.getX();
    }
    public int getY() {
        return this.centro.getY();
    }
    public int getRadio() {
        return this.radio;
    }
    public double calcularDistancia(Punto p2) {
        //Usamos el metodo para calcular distancia
        return centro.calcularDistancia(p2);
    }
    public double calcularArea(){
        //Calculamos el area pi*2^2
        return Math.pow(radio, 2)*Math.PI;
    } 
    public double calcularPerimetro(){
        //Calculamos el perimetro  2*pi*r
        return 2*pi*radio;
    }
}
